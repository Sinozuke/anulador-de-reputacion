<?php

if (!filter_has_var(INPUT_POST, "getD")) {
    die("<h1>401 - Unauthorized: Access is denied due to invalid credentials</h1>");
}

include '../config/DATA.php';

$con = mysqli_connect(HOST, USER, PASS, DB);
mysqli_set_charset($con, 'utf8');
$q = filter_input(INPUT_POST, "getD");

if (!$con) {
    die('Could not connect: ' . mysqli_error($con));
} else {

    switch ($q) {
        case "usuarios":
            $sql = "SELECT id,fname,lname,username,reputation,`Reputacion-valida` FROM `users` ORDER BY `Reputacion-valida`";
            $result = mysqli_query($con, $sql);

            echo "<table class=\"table table-striped table-hover\">
                <thead>
                <tr>
                <th>Nombre de Usuario</th>
                <th>Reputaccion</th>
                <th>Estado</th>
                <th>Accion.</th>
                </tr>
                </thead>";
            echo "<tbody>";
            while ($row = mysqli_fetch_array($result)) {
                echo "<tr>";
                echo "<td>" . $row['fname'] . " " . $row['lname'] . "(".$row['username'].")</td>";
                echo "<td>" . $row['reputation'] . "</td>";
                switch($row['Reputacion-valida']){
                    case 1:
                        echo "<td>Habilitada</td>";
                        echo "<td><button id=\"" . $row['id'] . "\" data-role=\"button\" onclick=\"checar(this.id,2)\">Inhabilitar</button></td>";
                        break;
                    case 2:
                        echo "<td>Inhabilitada</td>";
                        echo "<td><button id=\"" . $row['id'] . "\" data-role=\"button\" onclick=\"checar(this.id,1)\">Habilitar</button></td>";
                        break;
                }
                echo "</tr>";
            }
            echo "</tbody>";
            echo "</table>";
            break;
    }
    mysqli_close($con);
}