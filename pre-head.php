<?php
session_start();
if ($include_config == "a7ada8UReBepaqaMaMYGAGeqatA6abYbUBu3YRaZy4ySYLEHe9s") {

    function cabecera($title) {
        // Tipo de documento
        echo("<!doctype html>");

        // Lenguaje
        echo("<html lang=\"en\">");
        echo("<head>");

        // Charset estandar utf-8
        echo("<meta charset=\"utf-8\">");
        echo("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
        echo("<title>" . $title . "</title>");

        // Hojas de estilo para jQuery Mobile y jQuery Ui
        echo("<link rel=\"stylesheet\" href=\"" . INSTALLATION_URL . "admin/commons/css/jquery-ui.structure.css\">");
        echo("<link rel=\"stylesheet\" href=\"" . INSTALLATION_URL . "admin/commons/css/jquery-ui.theme.css\">");
        echo("<link rel=\"stylesheet\" href=\"" . INSTALLATION_URL . "admin/commons/css/mix.min.css\">");
        echo("<link rel=\"stylesheet\" href=\"" . INSTALLATION_URL . "admin/commons/css/jquery.mobile.icons.min.css\">");
        echo("<link rel=\"stylesheet\" href=\"" . INSTALLATION_URL . "admin/commons/css/bootstrap.css\">");
        echo("<link rel=\"stylesheet\" href=\"" . INSTALLATION_URL . "admin/commons/css/bootstrap.min.css\">");
        echo("<link rel=\"stylesheet\" href=\"" . INSTALLATION_URL . "admin/commons/css/bootstrap-theme.css\">");
        echo("<link rel=\"stylesheet\" href=\"" . INSTALLATION_URL . "admin/commons/css/bootstrap-theme.min.css\">");
        echo("<link rel=\"stylesheet\" href=\"" . INSTALLATION_URL . "admin/commons/js/jquery.mobile.structure-1.4.5.min.css\"/>");
        echo("<script src=\"" . INSTALLATION_URL . "admin/commons/js/jquery-3.1.1.js\"></script>");
        //echo("<script src=\"https://code.jquery.com/jquery-3.1.1.min.js\"></script>");
        echo("<script src=\"" . INSTALLATION_URL . "admin/commons/js/jquery-1.11.3.min.js\"></script>");
        echo("<script src=\"" . INSTALLATION_URL . "admin/commons/js/jquery.mobile-1.4.5.min.js\"></script>");


        // echo("<script src=\"".INSTALLATION_URL."admin/commons/js/jquery.mobile.custom.min.js\"></script>");
        echo("<link href=\"https://fonts.googleapis.com/css?family=Open+Sans\" rel=\"stylesheet\">");
        echo("<script src=\"" . INSTALLATION_URL . "admin/commons/js/jquery-ui.js\"></script>");
        // Scripts necesarios para redimension en caso de cambio de pantalla
        echo("<script>
				function desactivar(){
					$(\"a\").attr(\"rel\",\"external\");
				}
                                
                                function show(str){
                                    alert(str);
                                }
		
				$(function(){
						$(\"#tabs\").tabs();
						desactivar();
						});
					
					$(window).load(
						function(){
							reajustar();
						});						
					
					$(document).bind(\"mobileinit\", function () {
						$.mobile.ajaxEnabled = false;
					});
						
					function reajustar(){
						var ancho = $(window).width();
						var porcentaje = ancho * 0.23;
						if(ancho < 1400) porcentaje = ancho * 0.17;
						if(ancho < 1300) porcentaje = ancho * 0.12;
						if(ancho < 1200) porcentaje = ancho * 0.08;
						if(ancho < 1100) porcentaje = ancho * 0.05;
						if(ancho < 1000) porcentaje = ancho * 0.02;
						if(ancho < 900) porcentaje = 0;
						$(\"#full-container\").css(\"left\", porcentaje);
					}

					$(function() { $(window).resize(
							function(e){
								reajustar();
							});
						});
				</script>");
        // Hoja de estilos generales para la maquetacion
        echo("<script src=\"" . INSTALLATION_URL . "admin/commons/js/jquery.tablesorter.min.js\"></script>");
        echo("<link rel=\"stylesheet\" type=\"text/css\" href=\"" . INSTALLATION_URL . "admin/commons/css/mmads.css\"/>");
        echo("<link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">");
        echo("<link rel=\"stylesheet\" href=\"admin/modules/pagos/formst.css\">");
        echo("</head>");
        echo("<body data-role=\"page\">");
        echo("<div class=\"body-background\"></div>");
        echo("<div class=\"background-top\"></div>");
        echo("<div class=\"background-bottom\"></div>");
        echo("<div class=\"full-container\" id=\"full-container\">");
        echo("<div class=\"ui-panel-page-container-a\">");
        echo("<div class=\"ui-grid-a pad-l-15 pad-t-10 pad-r-15\">");
        echo("<div class=\"ui-block-a\">");
        echo("<image src=\"http://www.relato.gt/frontend/img/logo.svg\"/>");
        echo("</div>");
        echo("<div class=\"ui-block-b text-align-right\">");
        echo("<a href=\"" . INSTALLATION_URL . "cerrar\" target=\"_self\">");
        echo("<image src=\"" . INSTALLATION_URL . "admin/commons/images/botoncerrar.png\"/></a>");
        echo("</div>");
        echo("</div>");
        echo("<div class=\"ui-grid-solo pad-b-10 pad-r-15 text-align-right\">");
        echo("Bienvenido, Administrador");
        echo("</div>");
        echo("</div>");
        $string = $_SERVER['QUERY_STRING'];
        $string = str_replace("path=", "", $string);
        ?>
        <div class="pad-15 s-top-3 gray-bottom">
            <ul class="nav nav-tabs">
                <?php if ($string == "portada") { ?>
                    <li role="presentation" class="active"><a href="/portada">Dashboard</a></li>
                <?php } else { ?>
                    <li role="presentation"><a href="/portada">Dashboard</a></li>
                    <?php
                }
                if ($string == "emicion" || $string == "Emicion") {
                    ?>
                    <li role="presentation" class="active"><a href="/emicion">Emiciones</a></li>
                    <?php
                } else {
                    ?>
                    <li role="presentation"><a href="/emicion">Emiciones</a></li>
                <?php
                }
                if ($string == "Anulador" || $string == "anulador" || $string == "anulador-de-reputacion") {
                    ?>
                    <li role="presentation" class="active"><a href="/anulador-de-reputacion">Anulador</a></li>
                    <?php
                } else {
                    ?>
                    <li role="presentation"><a href="/anulador-de-reputacion">Anulador</a></li>
                <?php
                }
                if ($string == "pagos" || $string == "Pagos") {
                    ?>
                    <li role="presentation" class="active"><a href="/pagos">Administrador Pagos</a></li>
                        <?php
                    } else {
                        ?>
                    <li role="presentation"><a href="/pagos">Administrador Pagos</a></li>
            <?php } ?>
            </ul>
            <?php
        }

    }
    ?>
