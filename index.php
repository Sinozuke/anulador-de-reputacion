<?php
if ($include_config == "a7ada8UReBepaqaMaMYGAGeqatA6abYbUBu3YRaZy4ySYLEHe9s") {
    include('admin/modules/config/pre-head.php');
    include('admin/modules/config/post-body.php');

    cabecera('Relato - Anulador de Reputacion');
    ?>

    <div class="panel panel-default">
        <br/>
        <div class="panel-body">
            <script type="text/javascript" src="admin/modules/anulador-de-reputacion/programing.js"></script>
            <center>
                <h2>
                    Lista de Usuarios.
                </h2>
            </center>
            <div class="table-responsive">
                <div id="txtHint"></div>
            </div>
            <div id="dialog-confirm" title="Confirmar Emision" style='display:none;background-color: white'>
                <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Esta Seguro que desea Habilitar/Deshabilitar la reputacion?</p>
            </div>
        </div>
    </div>
    <?php
    pie();
}